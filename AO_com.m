classdef AO_com < handle
    properties
        hTask;
        hChan;
        sampRate = 10e3;
        
        m_TempsRepet;
        m_NombreRepet;
        m_JitterRepet;
        
        m_TimerRepet;
    end
    events
       e_StimDone; 
    end
    methods
        function obj = AO_com(DeviceName, TaskName, Channels)
            import dabs.ni.daqmx.*
            obj.hTask = Task(TaskName);
            
            obj.hChan = obj.hTask.createAOVoltageChan(DeviceName,Channels);
            obj.ConfigTiming('DAQmx_Val_FiniteSamps',10000,10000);
            obj.hTask.registerDoneEvent(@obj.OutputDone);
            obj.m_TimerRepet = timer;
        end
        
        function delete(obj)
           obj.hTask.stop;
           obj.hTask.delete;
        end 
        
        function ConfigTiming(obj, Type, freq, NbElements)
            obj.hTask.control('DAQmx_Val_Task_Unreserve');
            obj.hTask.control('DAQmx_Val_Task_Reserve');
            obj.hTask.cfgSampClkTiming(freq, Type, NbElements);
        end
        
        function SetRepTiming(obj, Tr, Nb, Jitter)
            obj.m_TempsRepet = Tr;
            obj.m_NombreRepet = Nb;
            obj.m_JitterRepet = Jitter;
            
            obj.m_TimerRepet.TimerFcn = @obj.TimerCallback;
            obj.m_TimerRepet.StartDelay = obj.m_TempsRepet + round(1000*obj.m_JitterRepet*rand)/1000;
        end
        
        function Send(obj, Signals)
           obj.hTask.writeAnalogData( Signals, inf, false); 
        end
        
        function Start(obj)
           start(obj.m_TimerRepet); 
        end
        
        function Stop(obj)
           obj.hTask.abort;
           stop(obj.m_TimerRepet);
        end
      
    end
    methods (Access = private)
        function TimerCallback(obj, src, evnt)
            if( obj.m_NombreRepet > 0 )
                obj.hTask.start;
                stop(obj.m_TimerRepet);
                obj.m_NombreRepet = obj.m_NombreRepet - 1;
            else
                obj.notify('e_StimDone');
            end
        end
        
        function OutputDone(obj, src, evnt)
            obj.m_TimerRepet.StartDelay = obj.m_TempsRepet + round(1000*obj.m_JitterRepet*rand)/1000;
            start(obj.m_TimerRepet);
            obj.hTask.stop;
        end
    end
end