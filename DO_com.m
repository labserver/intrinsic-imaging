classdef DO_com < handle
   properties
       % Default values
       sampRate = 10e3;
    end
    properties (SetAccess=private)
        hTask;
        hChan;
    end
    methods
        function obj = DO_com(deviceName, TaskName, chanID)
            import dabs.ni.daqmx.*
            obj.hTask = Task(TaskName);
            obj.hChan = obj.hTask.createDOChan(deviceName,chanID,'','DAQmx_Val_ChanPerLine');
            
            obj.ConfigTiming('DAQmx_Val_ContSamps',10000,10000);
        end
        
        function ConfigTiming(obj, Type, freq, NbElements)
            obj.hTask.cfgSampClkTiming(freq, Type, NbElements);
        end
        
        function delete(obj)
           obj.hTask.stop;
           obj.hTask.delete;
        end  
        
        function Send(obj, Signals)
           obj.hTask.writeDigitalData( Signals, inf, false); 
        end
        
        function Start(obj)
           obj.hTask.start; 
        end
        
        function Stop(obj)
           obj.hTask.stop; 
        end
        
    end
end