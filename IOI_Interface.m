classdef IOI_Interface < handle
   properties(SetAccess = private)
       
       % Increment de sauvegarde
       m_exp_index;
       m_tIndex;
       
       %Structure contenant les strings
       m_Strings;
       m_Langage;
       m_Menu_Fr;
       m_Menu_En;
       
       %La fenetre principale
       m_MainWindow;
       
       %Boutons
       m_pbStartStop;
       m_pbSelectSaveDir;
       m_pbAddExperiment;

       %Pannels
       m_CtrlIllum;
       m_CtrlStim;
       m_CtrlSave;
       
       %Checkboxes
       m_LEDrouge;
       m_LEDjaune;
       m_LEDverte;
       m_LEDbleu;
       m_SaveMAT;
       %m_SaveTXT;
       
       %%%
       %popupmenu
       %%%
       %Illum
       m_Freq_IOI;
       %Stim
       m_StimSource;
       
       %%%
       %edit
       %%%
       m_eFreqStim;
       m_eAcquiLengthStim;
       m_ePulseWidthStim;
       m_eTrainLengthStim;
       m_eTempRepStim;
       m_eNbRepStim;
       m_eJitterStim;
       m_eAmpliStim;
       m_SaveDirectory;
       m_FileName;
       
       %%%
       %Labels
       %%%
       m_lStreamPixString;
       m_lFreqFluo;
              
       %Illumination
       m_FreqIOI;
       m_FreqFluo;
       m_DataGo_Min;
       m_EstData;
       m_FSTextLabel;
       m_FreeSpace;
       
       %Stimulation
       m_lGenSource;
       m_lFreqStim;
       m_lAcquiLengthStim;
       m_lPulseWidthStim;
       m_lTrainLengthStim;
       m_lTempRepStim;
       m_lNbRepStim;
       m_lJitterStim;
       m_lAmpliStim;
       m_lUnitsFreqStim;
       m_lUnitsAcquiLengthStim; 
       m_lUnitsPulseWidthStim;
       m_lUnitsTrainLengthStim;
       m_lUnitsTempRepStim;
       m_lUnitsNbRepStim;
       m_lUnitsJitterStim;
       m_lUnitsAmpliStim;
             
   end
   events
      e_pbStart;
      e_pbStop;
      e_windowClosed;
      e_AddExpe;
   end
   methods
       %Constructeur
       function app = IOI_Interface()
           %%%
           %Fenetre principale
           %%%         
           app.m_MainWindow = figure('MenuBar','none','NumberTitle','off',...
               'Resize','off','Color',[ 0.8314 0.8157 0.7843],...
               'CloseRequestFcn',@app.closeApp,'Position',[100 50 500 500]);
           Menu = uimenu('Parent',app.m_MainWindow, 'Label', 'Langue');
           app.m_Menu_Fr = uimenu('Parent',Menu, 'Label', 'Fran�ais','Checked','On', 'Callback', @app.LangueFR);
           app.m_Menu_En = uimenu('Parent',Menu, 'Label', 'English', 'Callback', @app.LangueEN);
           app.m_Langage = 'Fr';
           app.m_Strings = StringGEN();
           
           
           app.m_lStreamPixString = uicontrol('Style', 'text',...
               'HorizontalAlignment', 'center', 'Position',[120 450 350 30],...
               'FontSize', 14, 'ForegroundColor','r');
           app.isStreampixRunning;
           
           
           %%%
           %Bouton Start/Stop
           %%%
           app.m_pbStartStop = uicontrol('Style', 'togglebutton',...
               'Position', [20 430 90 65], 'Callback', @app.pbStartStop_pressed);
           
           %%%
           %Panel pour le control de l'illum
           %%%
           app.m_CtrlIllum = uipanel('Parent', app.m_MainWindow,...
               'Position',[10/500 320/500 480/500 100/500]);
           a = axes('Parent', app.m_CtrlIllum, 'Visible', 'off','Position', [0, 0, 1, 1],...
               'Xlim', [0, 1], 'YLim', [0, 1]);
           app.m_LEDrouge = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'checkbox',...
               'Position', [10 60 55 20], 'Value', 1, ...
               'Callback', @app.LEDChanged);
           app.m_LEDjaune = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'checkbox',...
               'Position', [65 60 55 20], ...
               'Callback', @app.LEDChanged);
           app.m_LEDverte = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'checkbox',...
               'Position', [125 60 50 20], ...
               'Callback', @app.LEDChanged);
           app.m_Freq_IOI = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'popupmenu',...
               'String', '5Hz|10Hz|15Hz|30Hz|50Hz|60Hz', 'Position', [10 12 75 20], ...
               'Callback', @app.FreqChanged);
           app.m_FreqIOI = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 32 100 20]);
           
           line([0.375, 0.375], [0.1, 0.95], 'Parent', a, 'Color', 'k');
           
           app.m_LEDbleu = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'checkbox',...
               'Position', [190 60 65 20], ...
               'Callback', @app.LEDChanged);
           app.m_lFreqFluo = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'text',...
               'HorizontalAlignment', 'center', 'Position',[190 12 75 20],'String','5 Hz','ForegroundColor',[0.5 0.5 0.5]);
           app.m_FreqFluo = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[190 32 75 20]);
           
           line([0.575, 0.575], [0.1, 0.95], 'Parent', a, 'Color', 'k');
           
           app.m_EstData = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'text',...
               'Position', [280 20 70 30],'String', num2str(2*60*1/1024,3), 'FontSize', 18);
           app.m_DataGo_Min = uicontrol('Parent', app.m_CtrlIllum, 'Style', 'text',...
               'Position', [280 50 70 20]);
           app.m_FSTextLabel= uicontrol('Parent', app.m_CtrlIllum, 'Style', 'text',...
               'Position', [370 50 100 20]);       
           app.m_FreeSpace= uicontrol('Parent', app.m_CtrlIllum, 'Style', 'text',...
               'Position', [380 20 80 30],'FontSize', 18);
           
                      
           %%%
           %Panel pour le control de la Stimulation
           %%%
           app.m_CtrlStim =  uipanel('Parent', app.m_MainWindow,...
               'Position',[10/500 85/500 480/500 235/500],'Title','Stimulation');
           app.m_lGenSource = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 190 95 20],...
               'Callback',@obj.StimSourceChanged);
           app.m_StimSource = uicontrol('Parent', app.m_CtrlStim, 'Style', 'popupmenu',...
               'Position', [110 195 90 20], 'Callback', @app.StimSourceChanged,'Enable','on');
           app.m_lFreqStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 160 90 20]);
           app.m_eFreqStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 165 35 20], 'String', '5');
           app.m_lUnitsFreqStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 160 35 20], 'String', 'Hz');
           app.m_lAcquiLengthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 160 90 20],'Visible','off');
           app.m_eAcquiLengthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 165 35 20], 'String', '5',...
               'Visible','off');
           app.m_lUnitsAcquiLengthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 160 35 20], 'String', 'min',...
               'Visible','off');
           app.m_lPulseWidthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 135 90 20]);
           app.m_ePulseWidthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 140 35 20], 'String', '5');
           app.m_lUnitsPulseWidthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 135 35 20],'String','ms');
           app.m_lTrainLengthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 110 90 20]);
           app.m_eTrainLengthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 115 35 20], 'String', '5');
           app.m_lUnitsTrainLengthStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 110 35 20],'String','s');
           app.m_lTempRepStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 85 90 20]);
           app.m_eTempRepStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 90 35 20], 'String', '30');
           app.m_lUnitsTempRepStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 85 35 20],'String','s');
           app.m_lNbRepStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 60 90 20]);
           app.m_eNbRepStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 65 35 20], 'String', '10');
           app.m_lUnitsNbRepStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 60 35 20],'String','rep');
           app.m_lJitterStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 35 90 20]);
           app.m_eJitterStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 40 35 20], 'String', '3');
           app.m_lUnitsJitterStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 35 35 20],'String','s');
           app.m_lAmpliStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[10 10 90 20]);
           app.m_eAmpliStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'edit',...
               'HorizontalAlignment', 'center', 'Position',[110 15 35 20], 'String', '3');
           app.m_lUnitsAmpliStim = uicontrol('Parent', app.m_CtrlStim, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 10 35 20],'String','V');
           
           %%%
           %Panel pour le control de la Sauvegarde
           %%%
           app.m_CtrlSave = uipanel('Parent', app.m_MainWindow,...
               'Position',[10/500 5/500 480/500 75/500]);
           b = axes('Parent', app.m_CtrlSave, 'Visible', 'off','Position', [0, 0, 1, 1],...
               'Xlim', [0, 1], 'YLim', [0, 1]);
           app.m_SaveMAT = uicontrol('Parent', app.m_CtrlSave, 'Style', 'checkbox',...
               'Position', [10 35 40 20], 'Value', 1, 'String', '.mat');
%            app.m_SaveTXT = uicontrol('Parent', app.m_CtrlSave, 'Style', 'checkbox',...
%                'Position', [10 10 40 20], 'Value', 0, ...
%                'Callback', @app.FileTypeChanged, 'String', '.txt');
           
           line([0.125, 0.125], [0.1, 0.95], 'Parent', b, 'Color', 'k');
           
           app.m_pbSelectSaveDir = uicontrol('Parent', app.m_CtrlSave,'Position', [70 35 70 20],...
                'Callback', @app.pbSelectSaveDir_pressed);
           app.m_SaveDirectory = uicontrol('Parent', app.m_CtrlSave, 'Style', 'edit',...
               'HorizontalAlignment', 'left', 'Position',[150 35 150 20],...
               'String', ['C:\Projet\Data\' date '\'], 'Background', 'w');
           app.m_pbAddExperiment = uicontrol('Parent', app.m_CtrlSave, 'Position', [310 10 160 40],...
                'Callback', @app.pbAddExperiment_pressed);
           
           app.m_exp_index = size(ls(['C:\Projet\Data\' date '\E*']),1) + 1;
           app.m_tIndex = uicontrol('Parent', app.m_CtrlSave, 'Style', 'text',...
               'HorizontalAlignment', 'left', 'Position',[150 10 50 20],'String',['E' num2str(app.m_exp_index)]);
           
           %Pour ecrire le texte dans la bonne langue:          
           app.ChangeLangue();
           app.UpdateFreeSpace();
       end
       
       %Callback de fermeture de fenetre
       function closeApp(app, hObject, evendata)
           delete(app.m_MainWindow)
           app.notify('e_windowClosed');
       end
       
       %Callbacks pour bouttons
       function pbStartStop_pressed(obj, ~, ~)
           
           if( get(obj.m_pbStartStop, 'Value') == 1 )
               if( obj.isStreampixRunning )
                   % disp('Start');
                   obj.notify('e_pbStart');
                   set(obj.m_pbStartStop,'String',obj.m_Strings.m_pbStop.(obj.m_Langage));
               else
                   warndlg(obj.m_Strings.StreampixWarning.(obj.m_Langage));
                   set(obj.m_pbStartStop,'Value',0);
               end
           else
               %disp('Stop');
               obj.notify('e_pbStop');
               set(obj.m_pbStartStop,'String',obj.m_Strings.m_pbStart.(obj.m_Langage));       
           end
       end
       
       function ExternalStop(obj)
            set(obj.m_pbStartStop,'Value', 0);
            obj.pbStartStop_pressed;
       end
       
       function [R, G, Y, Ca, IOIF, FluoF] = GetIllumParam(obj)
          
          R =  get(obj.m_LEDrouge, 'Value');
          G = get(obj.m_LEDverte, 'Value');
          Y = get(obj.m_LEDjaune, 'Value');
          Ca = get(obj.m_LEDbleu, 'Value');
          
          switch get(obj.m_Freq_IOI, 'Value')
               case 1
                   IOIF = 5;
              case 2
                  IOIF = 10; 
              case 3
                  IOIF = 15;
               case 4
                   IOIF = 30;    
               case 5
                   IOIF = 50;
               case 6
                   IOIF = 60;
              otherwise
                  IOIF = 0;
          end
          
          FluoF = (R+G+Y)*IOIF;
          if( FluoF == 0 )
              FluoF = 60;
          end
        
       end
       
       function StimType = GetStimCtrl(obj)
          StimType = get(obj.m_StimSource, 'Value');  
       end
       
       function Longueur = GetAcquiLength(obj)
          Longueur = str2double(get(obj.m_eAcquiLengthStim, 'String'));
       end
       
       function  [F, W, D, Tr, Nb, Jitter, Ampl] = GetStimParam(obj)
           F = str2double(get(obj.m_eFreqStim, 'String')); 
           W = str2double(get(obj.m_ePulseWidthStim, 'String')); 
           D = str2double(get(obj.m_eTrainLengthStim, 'String')); 
           Tr = str2double(get(obj.m_eTempRepStim, 'String')); 
           Nb = str2double(get(obj.m_eNbRepStim, 'String')); 
           Jitter = str2double(get(obj.m_eJitterStim, 'String'));
           Ampl = str2double(get(obj.m_eAmpliStim, 'String'));
       end
       
       function [hasToSave, Folder, File] = GetSavingParams(obj)
           hasToSave = get(obj.m_SaveMAT, 'Value');
           Folder = get(obj.m_SaveDirectory, 'String');
           File = 'IOI';        
       end
   end
   methods (Access = private)
       function LangueFR(obj, src, evnt)
           obj.m_Langage = 'Fr';
           obj.ChangeLangue(); 
           set(obj.m_Menu_Fr, 'Checked', 'On');
           set(obj.m_Menu_En, 'Checked', 'Off');
       end
       function LangueEN(obj, src, evnt)
           obj.m_Langage = 'En';
           obj.ChangeLangue();           
           set(obj.m_Menu_Fr, 'Checked', 'Off');
           set(obj.m_Menu_En, 'Checked', 'On');
       end
       
       function UpdateFreeSpace(obj, src, evnt)
           path = get(obj.m_SaveDirectory,'String');
           FileObj = java.io.File(path(1:3));
           usable_Gb = round(FileObj.getUsableSpace/1000000)/1000;
           set(obj.m_FreeSpace,'String', usable_Gb);
       end
       
       function ChangeLangue(app)
          set(app.m_MainWindow,'Name',app.m_Strings.m_MainWindow.(app.m_Langage));
          set(app.m_pbStartStop,'String',app.m_Strings.m_pbStart.(app.m_Langage));       
          set(app.m_CtrlIllum, 'Title', app.m_Strings.m_CtrlIllum.(app.m_Langage));
          set(app.m_LEDrouge,'String',app.m_Strings.m_LEDrouge.(app.m_Langage)); 
          set(app.m_LEDjaune,'String',app.m_Strings.m_LEDjaune.(app.m_Langage));
          set(app.m_LEDverte,'String',app.m_Strings.m_LEDverte.(app.m_Langage)); 
          set(app.m_LEDbleu,'String',app.m_Strings.m_LEDbleu.(app.m_Langage));
          set(app.m_FreqIOI,'String',app.m_Strings.m_FreqIOI.(app.m_Langage)); 
          set(app.m_FreqFluo,'String',app.m_Strings.m_FreqFluo.(app.m_Langage));
          set(app.m_DataGo_Min,'String',app.m_Strings.m_DataGo_Min.(app.m_Langage));
          set(app.m_lGenSource,'String',app.m_Strings.m_lGenSource.(app.m_Langage));
          set(app.m_StimSource,'String',app.m_Strings.m_StimSource.(app.m_Langage));
          set(app.m_lFreqStim,'String',app.m_Strings.m_lFreqStim.(app.m_Langage));
          set(app.m_lPulseWidthStim,'String',app.m_Strings.m_lPulseWidthStim.(app.m_Langage));
          set(app.m_lTrainLengthStim,'String',app.m_Strings.m_lTrainLengthStim.(app.m_Langage));
          set(app.m_lTempRepStim,'String',app.m_Strings.m_lTempRepStim.(app.m_Langage));
          set(app.m_lNbRepStim,'String',app.m_Strings.m_lNbRepStim.(app.m_Langage));
          set(app.m_lJitterStim,'String',app.m_Strings.m_lJitterStim.(app.m_Langage));
          set(app.m_lAmpliStim,'String',app.m_Strings.m_lAmpliStim.(app.m_Langage));
          set(app.m_lAcquiLengthStim,'String',app.m_Strings.m_lAcquiLengthStim.(app.m_Langage));
          set(app.m_FSTextLabel,'String',app.m_Strings.m_FSTextLabel.(app.m_Langage));  
          set(app.m_CtrlSave, 'Title', app.m_Strings.m_CtrlSave.(app.m_Langage));
          set(app.m_pbSelectSaveDir, 'String', app.m_Strings.m_pbSelectSaveDir.(app.m_Langage));
          set(app.m_lStreamPixString, 'String', app.m_Strings.m_lStreamPixString.(app.m_Langage));
          set(app.m_pbAddExperiment, 'String', app.m_Strings.m_pbAddExperiment.(app.m_Langage));
       end
       
       function FreqChanged(app, src, evnt)
                     
           NbLED = get(app.m_LEDrouge, 'Value') + get(app.m_LEDjaune, 'Value')...
               + get(app.m_LEDverte, 'Value');
           switch get(app.m_Freq_IOI, 'Value')
               case 1
                   FreqIOI = 5;
               case 2
                   FreqIOI = 10;
               case 3
                   FreqIOI = 15;
               case 4                   
                   FreqIOI = 30;
               case 5
                   FreqIOI = 50;
               case 6
                   FreqIOI = 60;    
               otherwise
                   FreqIOI = 0;
           end
           
           FreqCa = (NbLED)*FreqIOI;
           if( get(app.m_LEDbleu, 'Value') )
              set(app.m_lFreqFluo,'String', [num2str(FreqCa) ' Hz'],'ForegroundColor','k');
           else
               set(app.m_lFreqFluo,'String', [num2str(FreqCa) ' Hz'],'ForegroundColor',[0.5 0.5 0.5]);
           end
           
           
           set(app.m_EstData,'String',num2str((FreqIOI*60*NbLED + get(app.m_LEDbleu, 'Value')*FreqCa*60)*2/1024,3));
       end
              
       function LEDChanged(app, src, evnt)
           NbLED = get(app.m_LEDrouge, 'Value') + get(app.m_LEDjaune, 'Value')...
               + get(app.m_LEDverte, 'Value');
           Fluo = get(app.m_LEDbleu, 'Value');
           
           if( NbLED == 3 )
               set(app.m_Freq_IOI,'Value',1);
               set(app.m_Freq_IOI,'String','5Hz|10Hz');
           elseif( NbLED == 2 && Fluo )
               set(app.m_Freq_IOI,'Value',1);
               set(app.m_Freq_IOI,'String','5Hz|10Hz');
           else
               set(app.m_Freq_IOI,'String','5Hz|10Hz|15Hz|30Hz|50Hz|60Hz');
           end
           app.FreqChanged(src, evnt);
       end
       
       function StimSourceChanged(obj, src, evnt)
           switch get(obj.m_StimSource, 'Value')
               case 1   %Logiciel
                   set(obj.m_lFreqStim,'Visible','on');
                   set(obj.m_lPulseWidthStim,'Visible','on');
                   set(obj.m_lTrainLengthStim,'Visible','on');
                   set(obj.m_lTempRepStim,'Visible','on');
                   set(obj.m_lNbRepStim,'Visible','on');
                   set(obj.m_lJitterStim,'Visible','on');
                   set(obj.m_lAmpliStim,'Visible','on');
                   set(obj.m_eFreqStim,'Visible','on');
                   set(obj.m_ePulseWidthStim,'Visible','on');
                   set(obj.m_eTrainLengthStim,'Visible','on');
                   set(obj.m_eTempRepStim,'Visible','on');
                   set(obj.m_eNbRepStim,'Visible','on');
                   set(obj.m_eJitterStim,'Visible','on');
                   set(obj.m_eAmpliStim,'Visible','on');
                   set(obj.m_lUnitsFreqStim,'Visible','on');
                   set(obj.m_lUnitsPulseWidthStim,'Visible','on');
                   set(obj.m_lUnitsTrainLengthStim,'Visible','on');
                   set(obj.m_lUnitsTempRepStim,'Visible','on');
                   set(obj.m_lUnitsNbRepStim,'Visible','on');
                   set(obj.m_lUnitsJitterStim,'Visible','on');
                   set(obj.m_lUnitsAmpliStim,'Visible','on');
                   set(obj.m_lAcquiLengthStim,'Visible','off');
                   set(obj.m_eAcquiLengthStim,'Visible','off');
                   set(obj.m_lUnitsAcquiLengthStim,'Visible','off');
        
               case 2   %Externe
                   set(obj.m_lFreqStim,'Visible','off');
                   set(obj.m_lPulseWidthStim,'Visible','off');
                   set(obj.m_lTrainLengthStim,'Visible','off');
                   set(obj.m_lTempRepStim,'Visible','off');
                   set(obj.m_lNbRepStim,'Visible','off');
                   set(obj.m_lJitterStim,'Visible','off');
                   set(obj.m_lAmpliStim,'Visible','off');
                   set(obj.m_eFreqStim,'Visible','off');
                   set(obj.m_ePulseWidthStim,'Visible','off');
                   set(obj.m_eTrainLengthStim,'Visible','off');
                   set(obj.m_eTempRepStim,'Visible','off');
                   set(obj.m_eNbRepStim,'Visible','off');
                   set(obj.m_eJitterStim,'Visible','off');
                   set(obj.m_eAmpliStim,'Visible','off');
                   set(obj.m_lUnitsFreqStim,'Visible','off');
                   set(obj.m_lUnitsPulseWidthStim,'Visible','off');
                   set(obj.m_lUnitsTrainLengthStim,'Visible','off');
                   set(obj.m_lUnitsTempRepStim,'Visible','off');
                   set(obj.m_lUnitsNbRepStim,'Visible','off');
                   set(obj.m_lUnitsJitterStim,'Visible','off');
                   set(obj.m_lUnitsAmpliStim,'Visible','off');
                   set(obj.m_lAcquiLengthStim,'Visible','on');
                   set(obj.m_eAcquiLengthStim,'Visible','on');
                   set(obj.m_lUnitsAcquiLengthStim,'Visible','on');
               case 3   %Resting State
                   set(obj.m_lFreqStim,'Visible','off');
                   set(obj.m_lPulseWidthStim,'Visible','off');
                   set(obj.m_lTrainLengthStim,'Visible','off');
                   set(obj.m_lTempRepStim,'Visible','off');
                   set(obj.m_lNbRepStim,'Visible','off');
                   set(obj.m_lJitterStim,'Visible','off');
                   set(obj.m_lAmpliStim,'Visible','off');
                   set(obj.m_eFreqStim,'Visible','off');
                   set(obj.m_ePulseWidthStim,'Visible','off');
                   set(obj.m_eTrainLengthStim,'Visible','off');
                   set(obj.m_eTempRepStim,'Visible','off');
                   set(obj.m_eNbRepStim,'Visible','off');
                   set(obj.m_eJitterStim,'Visible','off');
                   set(obj.m_eAmpliStim,'Visible','off');
                   set(obj.m_lUnitsFreqStim,'Visible','off');
                   set(obj.m_lUnitsPulseWidthStim,'Visible','off');
                   set(obj.m_lUnitsTrainLengthStim,'Visible','off');
                   set(obj.m_lUnitsTempRepStim,'Visible','off');
                   set(obj.m_lUnitsNbRepStim,'Visible','off');
                   set(obj.m_lUnitsJitterStim,'Visible','off');
                   set(obj.m_lUnitsAmpliStim,'Visible','off');
                   set(obj.m_lAcquiLengthStim,'Visible','on');
                   set(obj.m_eAcquiLengthStim,'Visible','on');
                   set(obj.m_lUnitsAcquiLengthStim,'Visible','on');
           end
       
       end
       
       function Status = isStreampixRunning(obj)
           [~, w] = dos('c:\Windows\System32\tasklist.exe /fi "imagename eq Streampix5-single.exe"');
           Status = ~isempty(strfind(w,'Streampix'));
           
           if( Status )
               set(obj.m_lStreamPixString,'Visible','off')
           else
               set(obj.m_lStreamPixString,'Visible','on')
           end
       end
       
       function pbSelectSaveDir_pressed(obj, src, evnt)
           NewFolder = uigetdir(get(obj.m_SaveDirectory,'String'));
           
           if( NewFolder ~= 0 )
               NewFolder = [NewFolder '\' date '\'];
               set(obj.m_SaveDirectory, 'String', NewFolder);
               obj.UpdateFreeSpace(src, evnt);
               obj.m_exp_index = size(ls([NewFolder '\E*']),1) + 1;
               set(obj.m_tIndex,'String',['E' num2str(obj.m_exp_index)]);
           end
       end
       
       function pbAddExperiment_pressed(obj, src, evnt)
           obj.notify('e_AddExpe');
           obj.m_exp_index = obj.m_exp_index +1;
           set(obj.m_tIndex,'String',['E' num2str(obj.m_exp_index)]);
       end
   end
end