classdef IOI_Soft < handle
   properties
      m_DO_PulseTr;
      m_AO_Stim;
      m_Interface;
      
      m_StimSig = 0;
      m_TimerRecord;
   end
   methods
       function obj = IOI_Soft
           
           obj.m_DO_PulseTr = DO_com('Dev1', 'DOPTr','port0/line0:4');
           obj.m_AO_Stim = AO_com('Dev1', 'AOStim',0);                  
           %Output Digitaux:
           %0: Trigger Camera
           %1: LED Rouge
           %2: LED Verte
           %3: LED Jaune
           %4: Calcium
                      
           %Interface Usag�
           obj.m_Interface = IOI_Interface;
           
           %Timer pour arreter l'acquisition
           obj.m_TimerRecord = timer;
           
           %Slots & Signals connections
           obj.m_Interface.addlistener('e_pbStart', @obj.Start);
           obj.m_Interface.addlistener('e_pbStop', @obj.Stop);
           obj.m_Interface.addlistener('e_windowClosed', @obj.CloseIOI);
           obj.m_Interface.addlistener('e_AddExpe', @obj.AddToExpList);
           obj.m_AO_Stim.addlistener('e_StimDone', @obj.StimDone);
       end
       
       function delete(obj)
          obj.Stop;
          obj.m_DO_PulseTr.delete;
          obj.m_AO_Stim.delete;
          delete(obj.m_TimerRecord);
          obj.m_Interface.delete;
       end
       
       function Start(obj, src, evnt)
          %Construction du patrons d'illumination
          obj.m_DO_PulseTr.Send(obj.GenerateIllumSig);
          %Stimulation
          if( obj.m_Interface.GetStimCtrl == 1 )
            obj.m_AO_Stim.Send(obj.GenerateStimSig);
            obj.m_AO_Stim.Start();
          else
             obj.m_TimerRecord.StartDelay = obj.m_Interface.GetAcquiLength*60;
             obj.m_TimerRecord.TimerFcn = @obj.StimDone;
             start(obj.m_TimerRecord);
          end        
          obj.m_DO_PulseTr.Start();  
       
       end
       
       function Stop(obj, src, evnt)
          obj.m_AO_Stim.Stop();
          obj.m_DO_PulseTr.Stop();
          obj.m_DO_PulseTr.Send(zeros(2,5));
          obj.m_DO_PulseTr.Start();
          obj.m_DO_PulseTr.Stop();
          stop(obj.m_TimerRecord);
          
          if( ishghandle(obj.m_Interface.m_MainWindow) )
              %Sauvegarde des parametres
              [Save, Folder, File] = obj.m_Interface.GetSavingParams;
              if( Save )
                  if( ~exist(Folder,'dir') )
                      mkdir(Folder);
                  end
                  Signaux = obj.GenerateIllumSig;
                  save([Folder filesep File '.mat'], 'Signaux');
              end
          end
       end
   end
   methods(Access = private)
       
       function Train = GenerateIllumSig(obj)
           [R, G, Y, B, IOIF] = obj.m_Interface.GetIllumParam();
          
           Temp_Integration = 9;
           Temps_Mort = 7.6667;
           freq_ech = 10000;
           
           TrainR = zeros(freq_ech, 1);
           TrainG = zeros(freq_ech, 1);
           TrainY = zeros(freq_ech, 1);
                       
           SommeLED = R + G + Y;
           if( SommeLED == 3 )
               TrainR(1:freq_ech/IOIF:end) = 1;
               TrainG(round(freq_ech/(IOIF*3))+1:round(freq_ech/IOIF):end) = 1;
               TrainY(round(2*freq_ech/(IOIF*3))+1:round(freq_ech/IOIF):end) = 1;
           elseif( SommeLED == 2 )
               if( R )
                   TrainR(1:freq_ech/IOIF:end) = 1;
                   if( G )
                        TrainG(0.01*freq_ech*round(1/(IOIF*2*0.01))+1:freq_ech/IOIF:end) = 1;
                   else
                       TrainY(0.01*freq_ech*round(1/(IOIF*2*0.01))+1:freq_ech/IOIF:end) = 1;
                   end
               else
                   TrainG(1:freq_ech/IOIF:end) =1;
                   TrainY(0.01*freq_ech*round(1/(IOIF*2*0.01))+1:freq_ech/IOIF:end) = 1;
               end
           elseif( SommeLED == 1 )
               if( R )
                   TrainR(1:freq_ech/IOIF:end) = 1;
                   
               elseif( G )
                   TrainG(1:freq_ech/IOIF:end) = 1;
               else
                   TrainY(1:freq_ech/IOIF:end) = 1;
               end
           end
       
           TrainB = zeros(freq_ech, 1);
           
           if( B )
                TrainB(round((Temps_Mort + Temp_Integration)*freq_ech/1000+1):end) = TrainR(1:end-round((Temps_Mort + Temp_Integration)*freq_ech/1000)) +...
                    TrainY(1:end-round((Temps_Mort + Temp_Integration)*freq_ech/1000)) + TrainG(1:end-round((Temps_Mort + Temp_Integration)*freq_ech/1000));
           end
           if( B && ~sum(TrainB) )
               TrainB(1:freq_ech/IOIF:end) = 1;
           end
           NbElements = round(Temp_Integration*freq_ech/1000);
           vecteur = [zeros(NbElements,1); ones(NbElements,1)];
           
           TrainR = conv(TrainR,vecteur,'same');
           TrainG = conv(TrainG,vecteur,'same');
           TrainY = conv(TrainY,vecteur,'same');
           TrainB = conv(TrainB,vecteur,'same');
           
            TrainCam = double(TrainR | TrainG | TrainY | TrainB);
     %       TrainG(1:end) = 1;
%            TrainCam = zeros(3000,1);
%            TrainCam(1:3:3000) = 1;
           
           Train = [TrainCam, TrainR, TrainG, TrainY, TrainB];
       end
       
       function Stim = GenerateStimSig(obj)
          [F, W, D, Tr, Nb, Jitter, Ampl] = obj.m_Interface.GetStimParam;
                    
          freq_ech = 10000;
          Stim = zeros(freq_ech*D,1);
          
          Stim(1:freq_ech/F:end) = Ampl;
          
          Stim = conv(Stim,[zeros(W*freq_ech/1000,1);ones(W*freq_ech/1000,1)],'same');
          Stim(end+1) = 0;
          
          obj.m_AO_Stim.ConfigTiming('DAQmx_Val_FiniteSamps',10000,length(Stim));
          obj.m_AO_Stim.SetRepTiming(Tr, Nb, Jitter);
       end   
       
       function StimDone(obj, ~, ~)
          obj.m_Interface.ExternalStop; 
       end
       
       function AddToExpList(obj,~,~)
           % First add new directory to experiment
           new_exp_dir = fullfile(get(obj.m_Interface.m_SaveDirectory,'String'),sprintf('E%2.2d',obj.m_Interface.m_exp_index));
           if( exist(new_exp_dir,'dir') )
               warndlg('Folder already exist. Please check', 'Add To Exp List error');
           else
               mkdir(new_exp_dir);
               % Then copy seq file to that directory
               movefile(fullfile(get(obj.m_Interface.m_SaveDirectory,'String'),'IOI.mat'),fullfile(new_exp_dir,'IOI_scaninfo.mat'));
               movefile(fullfile('C:\Projet\Data\Camera 1','IOI.seq'),fullfile(new_exp_dir,'IOI_scan.seq'));
               movefile(fullfile('C:\Projet\Data','AuxSignals.mat'),fullfile(new_exp_dir,'IOI_aux.mat'));
           end
       end
   end
   
   methods(Static)
       function CloseIOI(src, evnt)
           Test = evalin('base','whos');
           
           for ind = 1:size(Test,1)
               if( strcmp(Test(ind).class, 'IOI_Soft') )
                   if( evalin('base', ['isvalid(' Test(ind).name ')']) )
                       evalin('base', ['delete(' Test(ind).name ')'])
                   end
                   evalin('base', ['clear ' Test(ind).name]);
                   import dabs.ni.daqmx.*
                   Task.clearAllTasks;
               end
           end
       end
   end
end