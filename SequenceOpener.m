%% Initialisation Ouverture
Nom = 'C:\Projet\Data\Camera 1\IOI_5.seq';
SizeImage = memmapfile(Nom,'Offset',580,'Format','uint32','Repeat',1); 
NombreImage = memmapfile(Nom,'Offset',572,'Format','uint32','Repeat',1);
ImRes_XY = memmapfile(Nom,'Offset',548,'Format','uint32','Repeat',2);
data = memmapfile(Nom,'Offset',1024,'Format','uint16');

SizeImage = double(SizeImage.Data)/2;
ImRes_XY = double(ImRes_XY.Data);
NombreImage = double(NombreImage.Data);

FrameSeq = load('C:\Projet\Data\Test_5_1.mat');
AnalogIN = load('C:\Projet\Data\06-Feb-2014\Test_5_auxSignals.mat');

clear Nom;
%% Determination des blocs de Stim
FreqEch = 10000;
StimOn = find(AnalogIN.aux(:,1)>1000);
TempsInterStim = diff(StimOn);
LimiteStim = find(TempsInterStim > 1*FreqEch);
BlocStim(1,1) = StimOn(1);
BlocStim(1,2:length(LimiteStim)+1) = StimOn(LimiteStim+1);
BlocStim(2,1:length(LimiteStim)) = StimOn(LimiteStim);
BlocStim(2,end) = StimOn(end);
clear LimiteStim TempsInterStim StimOn;

%Separation des frames stim et non stim
Frames.Temps = find(diff(AnalogIN.aux(:,2))>10000);
for indF = 1:length(Frames.Temps)
   if( sum((Frames.Temps(indF) > BlocStim(1,:)).*(Frames.Temps(indF) < BlocStim(2,:))) )
       Frames.Stim(indF) = 1;
   else
       Frames.Stim(indF) = 0;
   end
end
clear indF;
%Separation des frames en couleurs (Red, Green, Yellow, Fluo)
F = find( diff(FrameSeq.Signaux(:,1)) > 0.1);
F = [1; F];
Red = find(diff(FrameSeq.Signaux(:,2)) > 0.1);
if( FrameSeq.Signaux(1,2) > 0 )
    Red = [1; Red];
end
Green = find(diff(FrameSeq.Signaux(:,3)) > 0.1);
if( FrameSeq.Signaux(1,3) > 0 )
    Green = [1; Green];
end
Yellow = find(diff(FrameSeq.Signaux(:,4)) > 0.1);
if( FrameSeq.Signaux(1,4) > 0 )
    Yellow = [1; Yellow];
end
Fluo = find(diff(FrameSeq.Signaux(:,5)) > 0.1);
if( FrameSeq.Signaux(1,5) > 0 )
    Fluo = [1; Fluo];
end

for indF = 1:length(F) 
    if( find(Red == F(indF)) )
        SequenceIllum(indF,1) = 'R';
    elseif( find(Green == F(indF)) )
        SequenceIllum(indF,1) = 'G';
    elseif( find(Yellow == F(indF)) )
        SequenceIllum(indF,1) = 'Y';
    elseif( find(Fluo == F(indF)) )
        SequenceIllum(indF,1) = 'F';    
    end
end
clear Red Yellow;

FramesR = [];
FramesG = zeros(489,1024,1024);
FramesY = [];
FramesB = [];
indx=1;
for indF = 1:489*3
    if(SequenceIllum(mod(indF-1,length(SequenceIllum))+1) == 'Y')
        FramesG(indx,:,:) = reshape((data.Data(1+(indF-1)*SizeImage:1+(indF-1)*SizeImage+ImRes_XY(1)*ImRes_XY(2)-1)),1024,1024);
        indx= indx+1;
    end
end

%% Separation des frames par couleurs
dSign = zeros(size(FrameSeq.Signaux,1), size(FrameSeq.Signaux,2));
dSign(1,:) = FrameSeq.Signaux(1,:);
dSign(2:end,:) = FrameSeq.Signaux(2:end,:) - FrameSeq.Signaux(1:end-1,:);



%%

Moy_Intensite = zeros(NombreImage,1);
Max_Intensite = zeros(NombreImage,1);
Min_Intensite = zeros(NombreImage,1);
Std_Intensite = zeros(NombreImage,1);
for indI = 1:NombreImage
    Image = data.Data(1+(indI-1)*SizeImage:1+(indI-1)*SizeImage+ImRes_XY(1)*ImRes_XY(2)-1);
    Moy_Intensite(indI) = mean(Image);
    Max_Intensite(indI) = max(Image);
    Min_Intensite(indI) = min(Image);
    Std_Intensite(indI) = std(single(Image));
end
%%
 temp = 0:10/300:10-10/300;

 figure;plot(temp,Moy_Intensite(1:300:end),'r.')
 hold;
 plot(temp,Moy_Intensite(2:300:end),'g.')
 plot(temp,Moy_Intensite(3:300:end),'y.')
 
 figure;plot(temp,100*Moy_Intensite(1:300:end)./mean(Moy_Intensite(1:3:end)),'r.')
 hold;
 plot(temp,100*Max_Intensite(1:300:end)./mean(Moy_Intensite(1:3:end)),'r--')
 plot(temp,100*Min_Intensite(1:300:end)./mean(Moy_Intensite(1:3:end)),'r--')

 plot(temp,100*Moy_Intensite(2:300:end)./mean(Moy_Intensite(2:3:end)),'g.')
 plot(temp,100*Max_Intensite(2:300:end)./mean(Moy_Intensite(2:3:end)),'g--')
 plot(temp,100*Min_Intensite(2:300:end)./mean(Moy_Intensite(2:3:end)),'g--')
 
 plot(temp,100*Moy_Intensite(3:300:end)./mean(Moy_Intensite(3:3:end)),'y.')
 plot(temp,100*Max_Intensite(3:300:end)./mean(Moy_Intensite(3:3:end)),'y--')
 plot(temp,100*Min_Intensite(3:300:end)./mean(Moy_Intensite(3:3:end)),'y--')
 
 
%%
fid = fopen('..\Data\Camera 1\10minutes.seq');

header = fread(fid,1024,'uint8');

fseek(fid,580,'bof');
SizeImageBytes = fread(fid,1,'long');

fseek(fid,-1,'eof');
NbIm = round((ftell(fid)-1024)/SizeImageBytes);
%NbIm = 1000;
Image = zeros(NbIm, 256, 256,'uint16');
TS_s = zeros(NbIm, 1,'uint16');
TS_ms = zeros(NbIm, 1,'uint16');
TS_us = zeros(NbIm, 1,'uint16');
for indI = 1:NbIm
    fseek(fid, 1024 + (indI-1)*SizeImageBytes,'bof');
    Image1 = uint16(fread(fid,256*256,'uint16'));
    Image(indI,:,:) = reshape(Image1,1024,1024);
    TS_s(indI) = fread(fid,1,'uint');
    TS_ms(indI) = fread(fid,1,'uint16');
    TS_us(indI) = fread(fid,1,'uint16');
end

TS_s = TS_s - min(TS_s);

fclose(fid)

%%
figure;plot(squeeze(mean(mean(Image,2),3)));


%%
DeltaI = zeros(1743,1024,1024);
for indI = 1:NbIm
    DeltaI(indI,:,:) = squeeze(Image(indI,:,:)) - uint16(Moyenne);
end
